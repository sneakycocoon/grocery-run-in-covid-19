import common.Factory;

import Rect;
import Constants;

class InfectionLayer extends h2d.Layers {
    var dailyInfectedText: h2d.Text;
    var color: h3d.Vector = h3d.Vector.fromColor(Constants.ColorBlack100);

    public function new(scene: h2d.Scene, dailyInfected: Int) {
        super(scene);

        var font = hxd.res.DefaultFont.get().clone();
        font.resizeTo(Constants.FontSize);

        this.dailyInfectedText = Factory.createH2dText(font,
                                                       '${dailyInfected} new cases of Corona virus detected yesterday',
                                                       Constants.InfectionPos + [Constants.RectPadding, Constants.RectPadding],
                                                       this.color);
        this.dailyInfectedText.maxWidth = Constants.RectSize1[0] - Constants.RectPadding * 2;

        this.add(new Rect(Constants.ColorGrey400, Constants.RectSize1, Constants.InfectionPos), 0);
        this.add(this.dailyInfectedText, 1);
    }

    public function update(dailyInfected: Int) {
        this.dailyInfectedText.text = '${dailyInfected} new cases of Corona virus detected yesterday';
    }
}