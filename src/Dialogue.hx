import common.Assets;
import common.Point2f;
import common.Factory;

import Rect;
import Place;
import Choice;
import Constants;

class DialogueLayer extends h2d.Layers {
    static final ChoicePadding: Float = 16;
    public var currentChoice: Choice;

    var color: h3d.Vector = h3d.Vector.fromColor(Constants.ColorBlack100);
    var position: Point2f;
    var place: Place;
    var placeNameText: h2d.Text;
    var dayText: h2d.Text;
    var eventText: h2d.Text;
    var dayPos: Point2f;
    var font: h2d.Font = hxd.res.DefaultFont.get().clone();
    var choiceTexts: Array<h2d.Text> = [];
    var choices: Array<Choice>;
    var currentChoiceIndex: Int;
    var currentChoiceText: h2d.Text;
    var postChoiceDialogue: h2d.Text;
    var assets: Assets;
    var choiceArrow: h2d.Object;
    var maxTextWidth: Int;
    var eventTextPos: Point2f;
    var currentEvent: Event;

    public function new(scene: h2d.Scene, place: Place, day: Int, assets: Assets) {
        super(scene);
        this.assets = assets;
        this.place = place;
        this.font.resizeTo(Constants.FontSize);
        this.maxTextWidth = Std.int(Constants.DialogueWidth - Constants.RectPadding2 * 2);

        var fontBig = hxd.res.DefaultFont.get().clone();
        fontBig.resizeTo(Constants.FontSizeBig);

        this.position = Constants.DialoguePos;
        this.dayPos = this.position + [Constants.RectPadding2, Constants.RectPadding2];
        this.dayText = this.createText('Day ${day}:', dayPos, fontBig);

        var placeNamePos = dayPos + [this.dayText.textWidth + 15.0, 0.0];
        this.placeNameText = this.createText(this.place.name, placeNamePos, fontBig);

        this.currentEvent = this.place.currentEvent;
        this.eventTextPos = this.dayPos + [0.0, 40.0];
        this.eventText = this.createText('${this.currentEvent.title}', eventTextPos, this.font, this.maxTextWidth);

        var choicesStartingPos = this.eventTextPos + [0, this.eventText.textHeight + 10.0];

        this.add(new Rect(Constants.ColorGrey400, [Constants.DialogueWidth, 190], this.position), 1);
        this.add(this.dayText, 2);
        this.add(this.placeNameText, 2);
        this.add(this.eventText, 2);

        this.addChoiceTexts(choicesStartingPos, this.currentEvent.choices);
        this.addChoiceArrow();
    }

    function addChoiceArrow() {
        this.choiceArrow = this.assets.getAsset("arrowLeft").getBitmap();
        this.add(choiceArrow, 2);
        this.updateChoiceArrowPos();
    }

    function updateChoiceArrowPos() {
        this.choiceArrow.x = this.currentChoiceText.x - ChoicePadding;
        this.choiceArrow.y = this.currentChoiceText.y;
    }

    function removeChoiceTexts() {
        if (this.choiceTexts.length > 0) {
            for (text in this.choiceTexts) {
                this.removeChild(text);
            }
            this.choiceTexts = [];
        }
    }

    function addChoiceTexts(prevPosition: Point2f, choices: Array<Choice>) {
        this.choices = choices;
        this.currentChoiceIndex = 0;
        this.currentChoice = this.choices[this.currentChoiceIndex];
        this.removeChoiceTexts();

        var currText:h2d.Text = null;
        for (choice in choices) {
            var index = choices.indexOf(choice);
            var position:Point2f;
            if (currText != null) {
                var currTextPos: Point2f = [currText.x, currText.y];
                position = currTextPos + [0, currText.textHeight + 5.0];
            } else {
                position = prevPosition + [15.0, 0.0];
            }
            var text = this.createText('${choice.text}',
                                       position,
                                       this.font,
                                       this.maxTextWidth);
            this.add(text, 2);
            this.choiceTexts.push(text);

            if (index == 0) {
                this.currentChoiceText = text;
            }
            currText = text;
        }
    }

    function createText(text:String, position:Point2f, font:h2d.Font, maxWidth:Int = null):h2d.Text {
        return Factory.createH2dText(font, text, position, this.color, maxWidth);
    }

    public function updateOnNewPlace(day: Int, place: Place) {
        this.place = place;
        this.currentEvent = this.place.currentEvent;
        this.choices = this.currentEvent.choices;
        this.dayText.text = 'Day ${day}:';
        this.placeNameText.text = place.name;

        this.currentChoiceIndex = 0;
        this.currentChoice = this.choices[this.currentChoiceIndex];

        this.removeChild(this.postChoiceDialogue);
        this.addEventSelectionUI();
    }

    function addEventSelectionUI() {
        trace(this.currentEvent);
        this.eventText = this.createText('${this.currentEvent.title}', this.eventTextPos, this.font, this.maxTextWidth);

        this.addChoiceTexts([this.eventText.x, this.eventText.y + this.eventText.textHeight + 10.0], this.choices);
        this.addChoiceArrow();
        this.add(this.eventText, 1);
    }

    function removeEventSelectionUI() {
        this.removeChild(this.choiceArrow);
        this.removeChild(this.eventText);
        this.removeChoiceTexts();
    }

    public function updatePostChoiceDialogue() {
        var text: String = '${this.currentChoice.consequence}';

        this.postChoiceDialogue = this.createText(text, this.eventTextPos, this.font, this.maxTextWidth);

        this.removeEventSelectionUI();
        this.add(this.postChoiceDialogue, 1);
    }

    public function moveToChoice(diff: Int) {
        if (diff < 0 && this.currentChoiceIndex == 0) {
            this.currentChoiceIndex = this.choices.length - 1;
        }
        else if (diff > 0 && this.currentChoiceIndex == this.choices.length - diff) {
            this.currentChoiceIndex = 0;
        }
        else {
            this.currentChoiceIndex += diff;
        }

        this.currentChoice = this.choices[this.currentChoiceIndex];
        this.currentChoiceText = this.choiceTexts[this.currentChoiceIndex];
        this.updateChoiceArrowPos();
    }
}
