import Event;

class Place {
    public var name: String;
    public var events: Array<Event>;
    public var currentEvent(get, null): Event;

    public function new(name: String, events: Array<Event>) {
        this.name = name;
        this.events = events;
    }

    public function get_currentEvent() {
        var randomIndex = Std.random(this.events.length);

        return this.events[randomIndex];
    }
}