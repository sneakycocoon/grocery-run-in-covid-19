import Event;

class Choice {
    public var text: String;
    public var isInfected: Bool;
    public var hunger: Int;
    public var money: Int;
    public var sanity: Int;
    public var localNews: String;
    public var consequence: String;
    public var triggerEvent: Event;
    public var people: Int;
    public var callback: () -> Void;

    public function new(text: String,
                        isInfected: Bool,
                        hunger: Int,
                        money: Int,
                        sanity: Int,
                        consequence: String,
                        people: Int = 0,
                        localNews: String = null,
                        triggerEvent: Event = null,
                        callback: () -> Void = null) {
        this.text = text;
        this.isInfected = isInfected;
        this.hunger = hunger;
        this.money = money;
        this.sanity = sanity;
        this.people = people;
        this.consequence = consequence;
        if (localNews != null) this.localNews = localNews;
        if (triggerEvent != null) this.triggerEvent = triggerEvent;
        if (callback != null) this.callback = callback;
    }
}