import common.Assets;
import common.Point2i;
import common.Factory;
import common.MathUtils;
import hxd.Rand;

import Event;
import Place;
import Events;
import Constants;

import events.GoToHospitalEvent;

import News;
import Stats;
import Infection;
import Symptoms;
import Dialogue;

class BasicScene extends common.Scene {
    var scene: h2d.Scene;

    var rand: hxd.Rand;

    var statsLayer: StatsLayer;
    var newsLayer: NewsLayer;
    var infectionLayer: InfectionLayer;
    var symptomsLayer: SymptomsLayer;
    var dialogueLayer: DialogueLayer;

    var events: Events;

    //var townPopulation: Int = 1000;
    var todayInfected: Int = 3;
    var townMap: Array<Place>;
    var currentPlaceIndex: Int = 0;

    final COMMON_ILLNESS = [Flu, Cold];

    public var totalInfected: Int = 0;
    public var currentDay: Int = 1;
    public var playerInfected: Int = 0;
    public var isGameOver: Bool;
    public var totalMoney: Int;
    public var totalSanity: Int;
    public var totalHunger: Int;
    public var isInfected: Bool;
    public var daySinceCovid: Int = 0;
    public var currentIllness: Illness = NoIllness;
    public var illnessRemainingDays: Int = Constants.IllnessRemainingDays;
    public var immuneRemainingDays: Int = Constants.ImmuneRemainingDays;
    public var gameOverReason: String;

    var isResolvingChoiceDialogue: Bool = false;
    var isUpPressed: Bool = false;
    var isDownPressed: Bool = false;
    var isEnterPressed: Bool = false;

    // events
    var goToHospitalEvent: Event;

    public function new(scene: h2d.Scene, assets: Assets) {
        this.scene = scene;

        this.rand = new Rand(Random.int(0, 100000));

        // setup game state
        this.townMap = this.setUpMap();
        this.setUpStats();
        this.setUpEvents();

        // set up all render layer
        this.statsLayer = new StatsLayer(this.scene, this.totalMoney, this.totalHunger, this.totalSanity);
        this.infectionLayer = new InfectionLayer(this.scene, this.todayInfected);
        this.dialogueLayer = new DialogueLayer(
                                               this.scene,
                                               this.townMap[this.currentPlaceIndex],
                                               this.currentDay,
                                               assets
                                               );
        this.symptomsLayer = new SymptomsLayer(this.scene);
        this.newsLayer = new NewsLayer(this.scene);
    }

    function setUpMap() {
        this.events = new Events();
        return [
                new Place("Home", events.homeEvents),
                //new Place("Bus Stop", events.randomEvents),
                //new Place("Shopping Street", events.randomEvents),
                new Place("Park", events.parkEvents),
                new Place("Supermarket", events.supermarketEvents)
                ];
    }

    function setUpEvents() {
        this.goToHospitalEvent = new GoToHospitalEvent(this.cureCommonIllness);
    }

    function setUpStats() {
        this.totalMoney = Constants.StartingMoney;
        this.totalSanity = Constants.StartingSanity;
        this.totalHunger = Constants.StartingHunger;
        this.totalInfected = this.todayInfected;
    }

    public function cureCommonIllness() {
        if (this.currentIllness == Cold || this.currentIllness == Flu) {
            var chance = MathUtils.random(0, 100);
            if (chance >= 10) {
                this.currentIllness = NoIllness;
                this.symptomsLayer.update(this.currentIllness);
                trace("Cured from Flu/Cold");
            }
        }
    }

    function moveToNextPlace() {
        if (this.currentPlaceIndex == this.townMap.length - 1) {
            this.currentPlaceIndex = 0;
            this.currentDay += 1;
            this.recalculateDailyStats();

            if (this.currentIllness != NoIllness) {
                if (this.events.homeEvents.indexOf(this.goToHospitalEvent) == -1) {
                    this.events.addEventToHome(this.goToHospitalEvent);
                }
            }

            this.statsLayer.update(this.totalMoney, this.totalHunger, this.totalSanity);
            this.infectionLayer.update(this.todayInfected);
            this.symptomsLayer.update(this.currentIllness);
            this.newsLayer.update();
        } else {
            this.currentPlaceIndex += 1;
        }

        var currentPlace = this.townMap[this.currentPlaceIndex];
        this.dialogueLayer.updateOnNewPlace(this.currentDay, currentPlace);
    }

    function moveToAboveChoice() {
        this.dialogueLayer.moveToChoice(-1);
    }

    function moveToBelowChoice() {
        this.dialogueLayer.moveToChoice(1);
    }

    function commitChoice() {
        var choice = this.dialogueLayer.currentChoice;
        this.totalMoney += choice.money;
        this.totalMoney = Std.int(Math.max(this.totalMoney * 1.0, 0.0));

        this.totalHunger += choice.hunger;
        this.totalHunger = Std.int(MathUtils.clampF(this.totalHunger * 1.0,
                                                    0.0,
                                                    100.0));

        this.totalSanity += choice.sanity;
        this.totalSanity = Std.int(MathUtils.clampF(this.totalSanity * 1.0,
                                                    0.0,
                                                    100.0));

        if (choice.isInfected && this.currentIllness != Covid) {
            var yourChance = MathUtils.random(0, 100);
            if (yourChance < Constants.CovidThresholdChance) {
                this.currentIllness = Covid;
                trace("Player has been infected with Covid " + Std.string(yourChance));
            }
        }

        if (choice.people > 0 && this.currentIllness == Covid) {
            this.playerInfected += Math.ceil(choice.people * Constants.InfectionRate);
            trace('Player infected: ${this.playerInfected}');
        }

        if (choice.localNews != null) {
            this.newsLayer.addToLocalNews(choice.localNews);
        }

        if (choice.callback != null) {
            choice.callback();
        }

        this.statsLayer.update(this.totalMoney, this.totalHunger, this.totalSanity);
        this.resolveChoiceDialogue();
    }

    function resolveChoiceDialogue() {
        this.isResolvingChoiceDialogue = true;
        this.dialogueLayer.updatePostChoiceDialogue();
    }

    function recalculateDailyStats() {
        this.totalHunger -= Constants.DailyHungerDrop;
        this.totalHunger = Std.int(MathUtils.clampF(this.totalHunger * 1.0,
                                                    0.0,
                                                    100.0));
        this.todayInfected = Math.ceil(this.totalInfected * Constants.InfectionRate) + this.playerInfected;
        this.totalInfected += this.todayInfected;

        this.recalcIllness();
    }

    function recalcIllness() {
        switch (this.currentIllness) {
        case NoIllness:
            if (this.immuneRemainingDays > 0) {
                this.immuneRemainingDays -= 1;
            }
            else if (this.immuneRemainingDays == 0) {
                var i = this.rand.random(this.COMMON_ILLNESS.length);
                this.currentIllness = this.COMMON_ILLNESS[i];
                this.immuneRemainingDays = Constants.ImmuneRemainingDays;
            }
        case Covid:
            this.daySinceCovid += 1;
            var deathChance = MathUtils.random(0, 100);
            if (deathChance < Constants.DeathFromCovidChance && this.daySinceCovid >= 14) {
                this.gameOverFromCovid();
            }
        default:
            if (this.illnessRemainingDays == 0) {
                this.illnessRemainingDays = Constants.IllnessRemainingDays;
                this.currentIllness = NoIllness;
            } else {
                this.illnessRemainingDays -= 1;
            }
        }

        trace('Day ${this.currentDay}: ${this.currentIllness}');
    }

    function gameOverFromCovid() {
        this.gameOverReason = "Die from Covid 19";
        this.gameOver();
    }

    function gameOver() {
        this.isGameOver = true;
    }

    override public function update(dt: Float) {
        if (this.totalMoney == 0) {
            this.gameOverReason = "No money, your mom disowns you.";
            this.gameOver();
        }

        if (this.totalHunger == 0) {
            this.gameOverReason = "Die from hunger";
            this.gameOver();
        }

        if (this.totalSanity == 0) {
            this.gameOverReason = "You went crazy and got locked up in an asylum";
            this.gameOver();
        }

        if (this.isResolvingChoiceDialogue && this.isEnterPressed) {
            this.moveToNextPlace();
            this.isEnterPressed = false;
            this.isResolvingChoiceDialogue = false;
        }
        else if (this.isEnterPressed) {
            this.commitChoice();
            this.isEnterPressed = false;
        }

        if (this.isUpPressed && !this.isResolvingChoiceDialogue) {
            this.moveToAboveChoice();
            this.isUpPressed = false;
        }

        if (this.isDownPressed && !this.isResolvingChoiceDialogue) {
            this.moveToBelowChoice();
            this.isDownPressed = false;
        }
    }

    override public function render(engine: h3d.Engine) {
        this.scene.render(engine);
    }

    override public function onEvent(event: hxd.Event) {
        if (event.kind == hxd.Event.EventKind.EKeyDown) {
            switch(event.keyCode) {
            case hxd.Key.W:
                this.isUpPressed = true;
            case hxd.Key.S:
                this.isDownPressed = true;
            case hxd.Key.SPACE:
                this.isDownPressed = true;
            case hxd.Key.ENTER:
                this.isEnterPressed = true;
            default:
            }
        }
        else if (event.kind == hxd.Event.EventKind.EKeyUp) {
            switch(event.keyCode) {
            case hxd.Key.W:
                this.isUpPressed = false;
            case hxd.Key.SPACE:
                this.isDownPressed = false;
            case hxd.Key.S:
                this.isDownPressed = false;
            case hxd.Key.ENTER:
                this.isEnterPressed = false;
            default:
            }
        }
    }
}
