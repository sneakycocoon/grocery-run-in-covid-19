import common.Point2f;

enum Illness {
    NoIllness;
    Flu;
    Cold;
    Covid;
}

class Constants {
    // color pallette
    public static final ColorGrey100 = 0xFFA4A4A4;
    public static final ColorGrey200 = 0xFFB4B4B4;
    public static final ColorGrey300 = 0xFFCDCDCD;
    public static final ColorGrey400 = 0xFFDCDCDC;
    public static final ColorTurquoise100 = 0xFF29734C;
    public static final ColorTurquoise200 = 0xFF359A65;
    public static final ColorTurquoise300 = 0xFF43BF7E;
    public static final ColorBlack100 = 0xFF222222;

    // font size
    public static final FontSize:Int = 12;
    public static final FontSizeBig:Int = 24;
    public static final BodyTextLineSpacing:Float = 20.0;

    // position of HUD
    public static final StatsPos:Point2f = [600.0, 400.0];
    public static final InfectionPos:Point2f = [600.0, 500.0];
    public static final NewsPos:Point2f = [10.0, 10.0];
    public static final SymptomsPos:Point2f = [600.0, 10.0];
    public static final DialoguePos:Point2f = [10.0, 400.0];

    // HUD size
    public static final RectSize1:Array<Int> = [190, 90];
    public static final RectSize2:Array<Int> = [190, 380];
    public static final RectPadding:Float = 10;
    public static final RectPadding2:Float = 15;
    public static final DialogueWidth:Int = 580;

    // initial resources
    public static final StartingMoney:Int = 500;
    public static final StartingSanity:Int = 100;
    public static final StartingHunger:Int = 90;

    // resources cap
    public static final SanityCap:Int = 100;
    public static final HungerCap:Int = 100;

    // stats
    public static final DailyHungerDrop:Int = 10;
    public static final IllnessRemainingDays:Int = 4;
    public static final ImmuneRemainingDays:Int = 1;
    public static final CovidThresholdChance:Int = 50; // over 100
    public static final DeathFromCovidChance:Int = 10; // over 100
    public static final InfectionRate:Float = 1.05;

    // symptoms
    public static final SymptomsPerDay = 1;
    static final CommonSymptoms:Array<String> = [
                                                 "fever",
                                                 "body aches",
                                                 "sore throat",
                                                 "tiredness",
                                                 "runny nose",
                                                 ];
    static final ColdOnlySymptoms:Array<String> = [
                                                   "cough",
                                                   "sneezing",
                                                   ];
    static final FluOnlySymptoms:Array<String> = [
                                                  "headache",
                                                  "diarrhea",
                                                  "short of breath",
                                                  "difficult breathing"
                                                  ];
    public static final FluSymptoms:Array<String> = FluOnlySymptoms.concat(CommonSymptoms);
    public static final ColdSymptoms:Array<String> = ColdOnlySymptoms.concat(CommonSymptoms);
    public static final CovidSymptoms:Array<String> = ColdOnlySymptoms.concat(FluOnlySymptoms).concat(CommonSymptoms);
}