package events;

import Choice;
import Event;

class GrabFoodEvent extends Event {
    public function new() {
        var title = "Why need to go out now when food delivery is so popular?";
        var choices = [
                        new Choice("You order a pizza through UberEats",
                                   false, 18, -20, -4,
                                   "Your mom scolded you big time for being a lazy ass. Asian people, she told you, eat hardship for a living."
                                   ),
                        new Choice("You despite all these MNCs and decide to ask your mom to cook for you instead",
                                   false, 7, 0, -10,
                                   "Your mom scolded you big time for being a lazy ass. Asian people, she told you, cook meal themselves."
                                   ),
                        new Choice("You think of asking your mom to cook. But she scold you last time. So you try to cook by yourself",
                                   false, -6, -15, 0,
                                   "You made a mess in the kitchen. Broke a few plates at least. Your mom scolded you again and deduct your daily allowance."
                                   ),
                       ];

        super(title, choices);
    }
}
