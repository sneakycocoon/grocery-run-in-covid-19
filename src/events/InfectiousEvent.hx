package events;

import Choice;
import Event;

class InfectiousEvent extends Event {
    public function new(people: Int) {
        var title = 'You see a crowd of ${people} here, you:';
        var choices = [
                        new Choice("Tell them to go home!",
                                   true, -15, 50, -10,
                                   "The gang don't agree with you. They hit you in the head and chase you away.",
                                   people,
                                   "Watch out for gang activity in the park. These otters are known to be very territorial."
                                   ),
                        new Choice("Join them to see what's happening.",
                                   true, 0, -100, 5,
                                   "They are a bunch of sales people. You succumb to their sweet talk and that lady's long leg. You end up buying a very expensive \"antique\" watch.",
                                   people,
                                   "A group of swindlers who sold fake watches has been caught by the police yesterday. Many have fallen victims to their sexy group leader."
                                   ),
                        new Choice("Ignore them.",
                                   false, 0, 0, -5,
                                   "You march on your road to solitary.",
                                   people
                                   ),
                        ];

        super(title, choices);
    }
}
