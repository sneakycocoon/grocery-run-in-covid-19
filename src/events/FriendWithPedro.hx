package events;

import Choice;
import Event;

class FriendWithPedroEvent extends Event {
    public function new() {
        var title = "You saw your childhood friend Pedro.";
        var choices = [
                       new Choice("You decide to hang out and talk about his recent sucsess in banana trading.",
                                  false, 0, 0, 10,
                                  "That was time well spent. You feel motivated to work harder. At least, some of your ideas are cooler than selling banana.",
                                  1
                                  ),
                       new Choice("You remember the last time you hang together, you ended up wasted on the street. So you hurry towards the market.",
                                  false, 0, 0, -15,
                                  "Pedro seemed disappointed that you ignored him. You heard his shouting from 2 blocks away.",
                                  1,
                                  "A successful fruit trader found dead in the neighbourhood. A suspect has been identified by the police."
                                   ),
                        ];

        super(title, choices);
    }
}
