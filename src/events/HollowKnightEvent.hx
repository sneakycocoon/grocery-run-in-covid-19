package events;

import Choice;
import Event;

class HollowKnightEvent extends Event {
    public function new() {
        var title = "You saw some movement in the bush.";
        var choices = [
                       new Choice("Hm! It could be a lost dog. You check it out.",
                                  false, 0, 0, -8,
                                  "Nothing. Just bugs. You thought you heard them taking about some City of Tears. You are not sure if you should be the one in tear for wasting your time."
                                  ),
                       new Choice("You throw a rock at the bush and ran away",
                                  false, -12, 0, 5,
                                  "A swarm of bees emerged and gave you a good chase. You are glad that you escaped. Although all the running made you pretty hungry."
                                   ),
                        ];

        super(title, choices);
    }
}
