package events;

import Choice;
import Event;

class SupermarketEvent extends Event {
    public function new(people: Int) {
        var title = 'You reach the supermarket. There are ${people} people around. You decide to:';
        var choices = [
                        new Choice("Get some food!",
                                   true, 20, -50, 0,
                                   "You subdue that growling stomach. That'll be it for today.",
                                   people
                                   ),
                        new Choice("Spend money in the entertainment corner",
                                   true, 0, -100, 20,
                                   "Yeah, nothing beats a good gaming session. You're a little bit saner now. Heading home!",
                                   people
                                   ),
                        new Choice("You change you mind and feel like heading home early",
                                   true, -10, 0, -10,
                                   "What a waste of time on the street today! And all that risk of infection for?",
                                   people
                                   )
                       ];

        super(title, choices);
    }
}
