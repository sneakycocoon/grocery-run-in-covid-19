package events;

import Choice;
import Event;

class OweMoneyEvent extends Event {
    public function new() {
        var title = "You saw Kimchi. That bitch owes you $200 bucks. She's coughing though";
        var choices = [
                        new Choice("Ah screw it. Not worth the risk",
                                   false, 0, 0, -12,
                                   "You are slightly annoyed by her nonchalant smile. You make sure to count interest the next time you meet her"
                                   ),
                        new Choice("Ohhh Kimchi, you look awesome today. By the way, about the money you borrow me last time...",
                                   true, 17, 170, 0,
                                   "You got your sweet money back! You are feeling generous and invite Kimchi out for a drink."
                                   ),
                       ];

        super(title, choices);
    }
}
