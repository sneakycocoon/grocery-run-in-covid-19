package events;

import Choice;
import Event;

class ProtestEvent extends Event {
    public function new(people: Int) {
        var title = "A huge gathering of anti-vax protestors! The development of Covid-19 vaccine surely stirs some people.";
        var choices = [
                        new Choice("Join them! You don't really believe in the movement, but creating chaos is your modus operandi.",
                                   true, 0, 0, -40,
                                   "Someone called the police. You end up in jail. Your mom has to bail you out. However, the grocery run still goes on.",
                                   people,
                                   "Police arrested a big crowd of protestors outside Townhall yesterday. Many are tested positive for the virus."
                                   ),
                        new Choice("You call the police! There's no sensible reason for a gathering this big during this social distancing time.",
                                   true, 20, 0, 10,
                                   "Become friend with the police. You was invited to their HQ for a sight seeing & cake cutting celebration.",
                                   100,
                                   "Some Policeman tested positive for Corona virus after dispersing the crowd outside Townhall. The whole Department is now closed for deep cleaning."
                                   ),
                        ];

        super(title, choices);
    }
}
