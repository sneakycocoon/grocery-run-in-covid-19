package events;

import Choice;
import Event;

class CharityEvent extends Event {
    public function new() {
        var title = "You saw some volunteers asking for help to deliver food to elderly house.";
        var choices = [
                        new Choice("You play the hero and decide to help",
                                   false, 10, 20, -10,
                                   "Hero doesn't always get the praise though. You get scolded for forgeting to apply hand sanitizer. Luckily, they give you some of their food.",
                                   20,
                                   "New Corona cluster found in elderly home. Hundreds are now under quarantined."
                                   ),
                        new Choice("You decide the market is of higher priority",
                                   false, 0, 0, -5,
                                   "[Game Designer] Do your have a soul man?"
                                   ),
                        ];

        super(title, choices);
    }
}
