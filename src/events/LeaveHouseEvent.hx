package events;

import Choice;
import Event;

class LeaveHouseEvent extends Event {
    public function new() {
        var title = "You decide to make a run for grocery at the nearby supermarket. Before you leave the house, you:";
        var choices = [
                        new Choice("Tap dance and whistle your favorite song. The world is lovely!",
                                   false, 0, 0, -5,
                                   "Your optimism received a good scolding from your mom, who doesn't want to risk her life for your carefree attitude. You feel sad."
                                   ),
                        new Choice("Pick up your cap. It's gonna be hot outside.",
                                   false, 0, 0, -10,
                                   "You can't find your cap so you use your mother's hat instead. You feel slightly embarassed for that."
                                   ),
                        new Choice("Wear your mask. You never trust what those officials say.",
                                   false, 0, 0, -2,
                                   "You are slightly worried that your mask reserve is running low. Last you heard, masks are the new Bitcoin now."
                                   ),
                        ];

        super(title, choices);
    }
}
