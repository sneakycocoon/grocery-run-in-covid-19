package events;

import Choice;
import Event;

class GoToHospitalEvent extends Event {
    public function new(callback: () -> Void) {
        var title = "You are a bit worried about those symptoms:";
        var choices = [
                        new Choice("Go get a medical check at the hospital!",
                                   false, 0, -100, 10,
                                   "It's nothing serious. The doctor said you should just rest at home. However, your mom never stops complaining about your grocery duty. So here you go again.",
                                   10,
                                   null,
                                   null,
                                   callback
                                   ),
                        new Choice("Nah, it's gonna be fine. You head to the supermarket again",
                                   false, 0, 0, -5,
                                   "Does the wind feel chillier than usual?"
                                   ),
                        ];

        super(title, choices);
    }
}
