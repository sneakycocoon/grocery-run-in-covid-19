package events;

import Choice;
import Event;

class SwitchSaleEvent extends Event {
    public function new(people: Int) {
        var title = 'There is a Switch Indie Summer Sale going on! The line looks like it has ${people} people. You decide to:';
        var games = [
                     "Hollow Knight",
                     "Dead Cell",
                     "Enter the Gungeon",
                     "Owlboy",
                     "Yoku Island Express",
                     "Darkest Dungeon",
                     "The Messenger",
                     "Katana Zero",
                     "Axiom Verge",
                     "Baba is You",
                     "Shovel Knight",
                     "Slay the Spire",
                     ];
        var choices = [
                        new Choice("Get your favorite indie title! Who cares about meat and veggie now when you have Slay the Spire now.",
                                   true, 0, -79, 15,
                                   "You imagine all the sweet time you gonna spend on your Switch tonight.",
                                   people
                                   ),
                        new Choice("Your mom gave you some stern warnings the last time you bought game over grocery. You bite you finger and move on",
                                   true, 7, -30, -8,
                                   "I have the food. I'm coming home. Stop calling me MAAAA.",
                                   people
                                   ),
                       ];

        super(title, choices);
    }
}
