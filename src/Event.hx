import Choice;

class Event {
    public var title: String;
    public var choices: Array<Choice>;

    public function new(title: String, choices: Array<Choice>) {
        this.title = title;
        this.choices = choices;
    }
}