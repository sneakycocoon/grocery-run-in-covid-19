import hxd.Rand;
import common.MathUtils;

import Choice;
import Event;
import events.Supermarket;
import events.LeaveHouseEvent;
import events.FriendWithPedro;
import events.InfectiousEvent;
import events.CharityEvent;
import events.HollowKnightEvent;
import events.SwitchSale;
import events.ProtestEvent;
import events.OweMoneyEvent;
import events.GrabFoodEvent;

class Events {
    var rand: hxd.Rand;

    public var homeEvents: Array<Event> = [
                                           new LeaveHouseEvent(),
                                           new GrabFoodEvent(),
                                           ];
    public var busStopEvents: Array<Event> = [];
    public var parkEvents: Array<Event> = [];
    public var supermarketEvents: Array<Event> = [];

    public var randomEvents: Array<Event> = [];

    public function new() {
        this.rand = new Rand(Random.int(0, 100000));

        this.createParkEvents();
        this.createSuperMarketEvents();
    }

    function createParkEvents() {
        this.parkEvents = [
                           new InfectiousEvent(MathUtils.random(10, 50)),
                           new CharityEvent(),
                           new FriendWithPedroEvent(),
                           new HollowKnightEvent(),
                           new ProtestEvent(MathUtils.random(100, 200)),
                           new OweMoneyEvent()
                           ];
    }

    function createSuperMarketEvents() {
        this.supermarketEvents = [
                                  new SupermarketEvent(MathUtils.random(50, 100)),
                                  new SwitchSaleEvent(MathUtils.random(20, 30))
                                  ];
    }

    public function addEventToHome(event: Event) {
        this.homeEvents.push(event);
    }
}