import hxd.Rand;

class Utils {
    static var rand:hxd.Rand = new Rand(Random.int(0, 100000));

    inline public static function shuffle<T>(a: Array<T>): Array<T> {
        var copy = a.copy();

        copy.sort(function(_, _) { return Utils.rand.random(10) - 5; });

        return copy;
    }
}