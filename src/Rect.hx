import common.Point2f;

class Rect extends h2d.Layers {
    var rect: h2d.Object;

    public function new(color, size: Array<Int>, position: Point2f) {
        super();

        this.rect = new h2d.Bitmap(h2d.Tile.fromColor(color, size[0], size[1]));
        this.rect.x = position.x;
        this.rect.y = position.y;

        this.add(this.rect, 0);
    }
}
