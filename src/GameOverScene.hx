import common.Factory;

class Layer extends h2d.Layers {
    var color: h3d.Vector;
    var headerText: h2d.Text;
    var daySurvivedText: h2d.Text;
    var playerInfectedText: h2d.Text;
    var totalInfectionText: h2d.Text;
    var reasonText: h2d.Text;
    var font: h2d.Font;

    public function new(scene: h2d.Scene, playerInfected: Int, currentDay: Int, totalInfection: Int, reason: String) {
        super(scene);

        this.color = h3d.Vector.fromColor(Constants.ColorGrey400);
        var fontBig = hxd.res.DefaultFont.get().clone();
        fontBig.resizeTo(48);

        var font = hxd.res.DefaultFont.get().clone();
        font.resizeTo(24);

        this.headerText = Factory.createH2dText(fontBig,
                                                "GAME OVER",
                                                [280.0, 200.0],
                                                this.color);

        this.reasonText = Factory.createH2dText(font,
                                                reason,
                                                [220.0, this.headerText.y + this.headerText.textHeight + 20.0],
                                                this.color);

        this.totalInfectionText = Factory.createH2dText(font,
                                                       'Total infected population: ${totalInfection}',
                                                       [220.0, this.reasonText.y + this.reasonText.textHeight],
                                                       this.color);

        this.playerInfectedText = Factory.createH2dText(font,
                                                       'Total # of people you infected: ${playerInfected}',
                                                       [this.totalInfectionText.x, this.totalInfectionText.y + this.totalInfectionText.textHeight],
                                                       this.color);

        this.daySurvivedText = Factory.createH2dText(font,
                                                    'Total # of day you survived: ${currentDay}',
                                                    [this.totalInfectionText.x, this.playerInfectedText.y + this.playerInfectedText.textHeight],
                                                    this.color);

        this.add(this.headerText, 0);
        this.add(this.reasonText, 0);
        this.add(this.totalInfectionText, 0);
        this.add(this.playerInfectedText, 0);
        this.add(this.daySurvivedText, 0);
    }
}

class GameOverScene extends common.Scene {
    public var scene: h2d.Scene;
    var layer: h2d.Layers;

    public function new(playerInfected: Int, currentDay: Int, totalInfection: Int, reason: String) {
        this.scene = new h2d.Scene();
        this.scene.scaleMode = Stretch(800, 600);

        this.layer = new Layer(this.scene, playerInfected, currentDay, totalInfection, reason);
    }

    override public function update(dt: Float) {
    }

    override public function render(engine: h3d.Engine) {
        this.scene.render(engine);
    }

    override public function onEvent(event: hxd.Event) {
    }
}