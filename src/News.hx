import common.Factory;
import common.Point2f;

import Rect;
import Constants;

class News {
    public static final HEADLINES = [
                                     "WHO announces epidemic status for Corona virus - advise medical mask is not necessary unless people are ill.",
                                     "China officials say eating bat is not to blame for the virus' origin.",
                                     "Chief Medical Zhang raises concern over slow response to the virus outbreak, questiones central government's ability.",
                                     "Hungry, jobless Americans turning to food banks to survive COVID-19 pandemic.",
                                     "Whistle blower Zhang Tee Ching is missing, according to her family latest interview...",
                                     "Hundreds pour to the street to march against US COVID-19 rules. New cluster found near protest site.",
                                     "Trump warns China could face 'consequences' over COVID-19 pandemic. President Xi caught laughing out loud in public.",
                                     "Just-In: Number of coronarivus cases in Egypt passes 3,000. Yemen intervention mission to be carried out regardless.",
                                     "France COVID-19 death toll increases, at slowing rate. Champaign are out of stock.",
                                     "UK PM Johnson has had some contact with ministers during COVID-19 recovery.",
                                     "Tens of thousands defy Bangladesh lockdown for top Islamic preacher's funeral.",
                                     "Op-Ed: Costco priced a 82 inch Samsung TV for $1,200, I don’t think that was a coincidence.",
                                     "Putin orders a lower daily coronavirus projection as Russia's tally nears 37,000.",
                                     "Vietnam Communist Party to donate 5 million tons of rice to China. Riot broke over concern of its own rice shortage.",
                                     "Op-Ed: Small weekend is over.. now entering big weekend",
                                     "The Wuhan lab at the core of a coronavirus controversy",
                                     "Scientists try 'cloud brightening' to protect Great Barrier Reef",
                                     "Op-Ed: China to celebrate its 63rd day of CNY.",
                                     "Coronavirus prompts Canada to roll out safe drugs for street users",
                                     "Air pollution at the lowest for the past 10 years. Environmentalists rejoice over the Corona God",
                                     "Fyre Festival organizers promoted to manage 2020 planning",
                                     "Animal Crossing to become the fastest selling Switch game in Q1, 2020",
                                     "5-Second rule shortened to 3 seconds amid growing health concerns over the pandemic",
                                     "Prime Minister wife found infected with Covid. Last contact traced back to Deputy Prime Minister",
                                     "PornHub becomes the next unicorn within 3 week of lock down, surpassing Facebook & Google to be the most valuable company.",
                                     "Movie to watch: Panic at Costco",
                                     "Italian's birthname registration surges with demand for the name Quarantino",
                                     "Have you stock up your toilet paper?",
                                     "Dog to celebrate their world wide 1 month holiday with hooman",
                                     "Husband and wife divorced over disagreement of what to cook for their date night",
                                     ];
}

class NewsLayer extends h2d.Layers {
    var bodyTexts: Array<h2d.Text> = [];
    var bodyTextMaxWidth: Int;
    var color: h3d.Vector;
    var headerText: h2d.Text;
    var font: h2d.Font;
    var contents: Array<String>;
    var localNews: Array<String> = [];
    var localNewsTexts: Array<h2d.Text> = [];

    static final NewsPerDay: Int = 2;

    public function new(scene: h2d.Scene) {
        super(scene);
        this.bodyTextMaxWidth = Std.int(Constants.DialogueWidth - Constants.RectPadding2 * 2);
        this.color = h3d.Vector.fromColor(Constants.ColorBlack100);
        this.font = hxd.res.DefaultFont.get().clone();
        this.font.resizeTo(Constants.FontSize);

        var fontBig = hxd.res.DefaultFont.get().clone();
        fontBig.resizeTo(Constants.FontSizeBig);

        this.headerText = Factory.createH2dText(fontBig,
                                               "In the News",
                                               Constants.NewsPos + [Constants.RectPadding2, Constants.RectPadding2],
                                               this.color);

        this.contents = News.HEADLINES.copy();
        this.bodyTexts = this.createBodyTexts(contents.splice(0, NewsLayer.NewsPerDay));

        this.add(new Rect(Constants.ColorGrey400, [Constants.DialogueWidth, 380], Constants.NewsPos), 0);
        this.add(this.headerText, 1);

        for (t in this.bodyTexts) {
            this.add(t, 1);
        }
    }

    function createBodyTexts(contents: Array<String>):Array<h2d.Text> {
        var texts = [];
        if (contents.length == 0) return texts;

        var headerPos:Point2f = [this.headerText.x, this.headerText.y + this.headerText.textHeight + 10.0];

        var currText:h2d.Text = null;
        for (c in contents) {
            var i = contents.indexOf(c);
            var position:Point2f;
            if (currText != null) {
                var currTextPos: Point2f = [currText.x, currText.y];
                position = currTextPos + [0.0, currText.textHeight + 5.0];
            } else {
                position = headerPos;
            }
            var text = Factory.createH2dText(this.font,
                                             '* ${c}',
                                             position,
                                             this.color,
                                             this.bodyTextMaxWidth
                                             );
            currText = text;
            texts.push(text);
        }
        currText = null;

        return texts;
    }

    function removeCurrentTexts() {
        if (this.bodyTexts.length > 0) {
            for (c in this.bodyTexts) {
                this.removeChild(c);
            }
        }

        if (this.localNewsTexts.length > 0) {
            for (c in this.localNewsTexts) {
                this.removeChild(c);
            }
        }
    }

    public function addToLocalNews(news) {
        this.localNews.push(news);
    }

    function createLocalNewsTexts(news: Array<String>):Array<h2d.Text> {
        var texts = [];
        if (news.length == 0) return texts;

        var lastBodyText = this.bodyTexts[this.bodyTexts.length - 1];
        var prevPosition:Point2f = [lastBodyText.x, lastBodyText.y + lastBodyText.textHeight + 20.0];

        var currText:h2d.Text = null;
        for (c in news) {
            var i = news.indexOf(c);
            var position:Point2f;
            if (currText != null) {
                var currTextPos: Point2f = [currText.x, currText.y];
                position = currTextPos + [0.0, currText.textHeight + 5.0];
            } else {
                position = prevPosition;
            }
            var text = Factory.createH2dText(this.font,
                                             '* Local news: ${c}',
                                             position,
                                             this.color,
                                             this.bodyTextMaxWidth
                                             );
            currText = text;
            texts.push(text);
        }
        currText = null;

        return texts;
    }

    public function update() {
        var news:Array<String> = contents.splice(0, NewsLayer.NewsPerDay);
        if (news.length == 0) {
            news = [
                    "The world went into hiding"
                    ];
        }
        this.removeCurrentTexts();
        this.bodyTexts = this.createBodyTexts(news);
        this.localNewsTexts = this.createLocalNewsTexts(this.localNews);

        for (t in this.bodyTexts) {
            this.add(t, 1);
        }

        for (t in this.localNewsTexts) {
            this.add(t, 1);
        }

        // reset local news
        this.localNews = [];
    }
}
