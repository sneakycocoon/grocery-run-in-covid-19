import common.Factory;

import Rect;
import Constants;

class StatsLayer extends h2d.Layers {
    var moneyText: h2d.Text;
    var hungerText: h2d.Text;
    var sanityText: h2d.Text;

    public function new(scene: h2d.Scene, money: Int, hunger: Int, sanity: Int) {
        super(scene);

        var color: h3d.Vector = h3d.Vector.fromColor(Constants.ColorTurquoise100);
        var origin = Constants.StatsPos + [Constants.RectPadding, Constants.RectPadding];
        var lineSpacing = 12.0;
        var font = hxd.res.DefaultFont.get().clone();

        font.resizeTo(Constants.FontSize);

        this.moneyText = Factory.createH2dText(font,
                                               this.getMoneyText(money),
                                               origin,
                                               color);
        this.hungerText = Factory.createH2dText(font,
                                                this.getHungerText(hunger),
                                                origin + [0, Constants.FontSize + lineSpacing],
                                                color);
        this.sanityText = Factory.createH2dText(font,
                                                this.getSanityText(sanity),
                                                origin + [0, (Constants.FontSize + lineSpacing) * 2],
                                                color);

        this.add(new Rect(Constants.ColorGrey400, Constants.RectSize1, Constants.StatsPos), 0);
        this.add(moneyText, 1);
        this.add(hungerText, 1);
        this.add(sanityText, 1);
    }

    function getMoneyText(money: Int) {
        return 'Balance: ${"$"}${money}';
    }

    function getHungerText(hunger: Int) {
        return 'Hunger: ${hunger}/${Constants.HungerCap}';
    }

    function getSanityText(sanity: Int) {
        return 'Sanity: ${sanity}/${Constants.SanityCap}';
    }

    public function update(money: Int, hunger: Int, sanity: Int) {
        this.moneyText.text = this.getMoneyText(money);
        this.hungerText.text = this.getHungerText(hunger);
        this.sanityText.text = this.getSanityText(sanity);
    }
}
