import hxd.Rand;
import common.Point2f;
import common.Factory;

import Rect;
import Constants;

class SymptomsLayer extends h2d.Layers {
    var font: h2d.Font = hxd.res.DefaultFont.get().clone();
    var color: h3d.Vector = h3d.Vector.fromColor(Constants.ColorBlack100);
    var colorFade: h3d.Vector = h3d.Vector.fromColor(Constants.ColorGrey100);
    var illnessColor: h3d.Vector = h3d.Vector.fromColor(Constants.ColorTurquoise100);
    var symptomStartPos: Point2f;
    var symptomTexts: Array<h2d.Text>;
    var hintText: h2d.Text;
    var currentSymptoms: Array<String> = [];
    var rand: hxd.Rand;

    public function new(scene: h2d.Scene) {
        super(scene);
        this.rand = new Rand(Random.int(0, 100000));
        this.font.resizeTo(Constants.FontSize);

        var fontBig = hxd.res.DefaultFont.get().clone();
        fontBig.resizeTo(Constants.FontSizeBig);

        var headerPos = Constants.SymptomsPos + [Constants.RectPadding, Constants.RectPadding];
        var headerText = Factory.createH2dText(fontBig,
                                               "Symptoms",
                                               headerPos,
                                               this.color);

        this.symptomStartPos = headerPos + [0.0, 48];
        this.symptomTexts = this.getSymptomTexts();

        var lastSymptomText = this.symptomTexts[this.symptomTexts.length - 1];
        this.hintText = Factory.createH2dText(font,
                                              "Do you have Corona virus??? Or do you just have a cold?",
                                              [lastSymptomText.x, lastSymptomText.y + 40.0],
                                              this.color);
        this.hintText.maxWidth = Constants.RectSize2[0] - Constants.RectPadding * 2;

        this.add(new Rect(Constants.ColorGrey400, Constants.RectSize2, Constants.SymptomsPos), 0);
        this.add(headerText, 1);
        for (t in this.symptomTexts) {
            this.add(t, 1);
        }
        this.add(this.hintText, 1);
    }

    function getSymptomTexts() {
        var texts = [];
        for (s in Constants.CovidSymptoms) {
            var i = Constants.CovidSymptoms.indexOf(s);
            var text = Factory.createH2dText(this.font,
                                             s,
                                             this.symptomStartPos + [0.0, Constants.BodyTextLineSpacing * i],
                                             this.colorFade
                                             );
            texts.push(text);
        }

        return texts;
    }

    // trigger everyday
    public function update(illness) {
        var applicableSymptoms:Array<String> = [];

        switch (illness) {
        case NoIllness:
            applicableSymptoms = Constants.CovidSymptoms.copy();
            this.currentSymptoms = [];
        case Flu:
            applicableSymptoms = Constants.FluSymptoms.copy();
        case Cold:
            applicableSymptoms = Constants.ColdSymptoms.copy();
        case Covid:
            applicableSymptoms = Constants.CovidSymptoms.copy();
        default:
        }

        if (illness != NoIllness) {
            var excludeCurrentSymptoms = applicableSymptoms.filter(function (s) {
                    return this.currentSymptoms.indexOf(s) == -1;
                });

            var symptoms = this.shuffle(excludeCurrentSymptoms);

            for (i in 0...Constants.SymptomsPerDay) {
                this.currentSymptoms.push(symptoms.pop());
            }
        }

        for (t in this.symptomTexts) {
            if (this.currentSymptoms.indexOf(t.text) > -1){
                t.color = this.illnessColor;
            }
            else {
                t.color = this.colorFade;
            }
        }
    }

    function shuffle<T>(a: Array<T>): Array<T> {
        var copy = a.copy();

        copy.sort(function(_, _) { return this.rand.random(10) - 5; });

        return copy;
    }
}
